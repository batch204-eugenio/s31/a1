const http = require(`http`);
const port = 3000;
const server = http.createServer((req, res) => {
	if(req.url == `/login`) {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end(`Welcome to the login page`)
	}
	else if (req.url == `/register`) {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end(`I'm sorry the page you are looking for cannot be found.`);
	}
	else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end(`error 404`);
	}
});
server.listen(port);
console.log(`Server is now accessible at localhost: ${port}.`)